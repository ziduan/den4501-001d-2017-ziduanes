﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Reservation.Presentation.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/IndexStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container">
        <div class="form-group">
            <a href="Maintainer.aspx">
                <asp:Label CssClass="badge badge-info" ID="Label1" runat="server" Text="¡Haz clic sobre mí para reservar!"></asp:Label>
            </a>
        </div>
    </div>
</asp:Content>
