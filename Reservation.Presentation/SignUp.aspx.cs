﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Reservation.Business;

namespace Reservation.Presentation
{
    public partial class SignUp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            Student stu = new Student()
            {
                Rut = txtRut.Text,
                FName = txtName.Text,
                LName = txtLastName.Text
            };

            StudentAccount user = new StudentAccount()
            {
                RUT = txtRut.Text,
                Username = txtUserName.Text,
                Password = txtPassword.Text
            };

            if (stu.Create() && user.Create())
            {
                Response.Redirect("Index.aspx");
            }
        }
    }
}