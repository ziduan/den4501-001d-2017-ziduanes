﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Reservation.Business;
using System.Web.Security;

namespace Reservation.Presentation
{
    public partial class LoginPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lgnUser_Authenticate(object sender, AuthenticateEventArgs e)
        {
            StudentAccount user = new StudentAccount()
            {
                Username = lgnUser.UserName,
                Password = lgnUser.Password
            };

            if (user.Authenticate())
            {
                Student stu = new Student()
                {
                    Rut = user.RUT
                };
                stu.Read();
                Session["RUT"] = stu.Rut;
                FormsAuthentication.RedirectFromLoginPage(stu.FName + " " + stu.LName, false);
            }
        }
    }
}