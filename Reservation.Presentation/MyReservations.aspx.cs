﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Reservation.Business;

namespace Reservation.Presentation
{
    public partial class MyReservations : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadMyReservations();
                LoadSports();
            }
        }

        private void LoadSports()
        {
            CollectionManager col = new CollectionManager();
            foreach (var s in col.GetSports())
            {
                ddlSport.Items.Add(new ListItem()
                {
                    Text = s.Description,
                    Value = s.ID.ToString()
                });
            }
        }

        private void LoadMyReservations()
        {
            CollectionManager col = new CollectionManager();
            foreach (var r in col.GetMyReservations((string)Session["RUT"]))
            {
                ddlMyReservations.Items.Add(
                    new ListItem()
                    {
                        Text = r.Description(),
                        Value = r.SportID.ToString()
                    });
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Business.Reservation mr = new Business.Reservation()
                {
                    SportID = int.Parse(ddlSport.SelectedValue),
                    Date = DateTime.Parse(ddlMyReservations.SelectedItem.Text.Split('_')[0])
                };

                if (mr.Delete())
                {
                    lblMessage.Text = "Reserva eliminada";
                }
                else
                {
                    lblMessage.Text = "No se pudo eliminar la reserva";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtDate.Text.ToString() != string.Empty)
                {
                    Sport s = new Sport()
                    {
                        Description = ddlMyReservations.SelectedItem.Text.Split('_')[1]
                    };
                    s.ReadThroughDescription();
                    Business.Reservation mr = new Business.Reservation()
                    {
                        Date = DateTime.Parse(ddlMyReservations.SelectedItem.Text.Split('_')[0]),
                        SportID = s.ID
                    };

                    if (mr.Update(DateTime.Parse(txtDate.Text), int.Parse(ddlSport.SelectedValue)))
                    {
                        LoadMyReservations();
                        lblMessage.Text = "Reserva Actualizada";
                        Response.Redirect("DataMaster.aspx");
                    }
                    else
                    {
                        lblMessage.Text = "No se pudo Actualizar la reserva";
                    }
                }
                else
                {
                    lblMessage.Text = "Indique fecha por favor";
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
    }
}