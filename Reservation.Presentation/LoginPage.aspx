﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="Reservation.Presentation.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous" />
    <link href="css/LoginPageStyle.css" rel="stylesheet" />
    <link href="css/GlobalStyle.css" rel="stylesheet" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Autenticación</title>
</head>
<body>
    <form class="lgn-form" id="form1" runat="server">
        <div class="centered-box">
            <div class="container">
                <asp:Login ID="lgnUser" runat="server" OnAuthenticate="lgnUser_Authenticate" DisplayRememberMe="False">
                    <LayoutTemplate>
                        <div class="form-group">
                            <asp:TextBox CssClass="form-control" placeholder="Nombre de usuario" ID="UserName" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="validator-text" ID="UserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="*El nombre de usuario es obligatorio." ToolTip="El nombre de usuario es obligatorio." ValidationGroup="lgnUser"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <asp:TextBox CssClass="form-control" placeholder="Contraseña" ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator CssClass="validator-text" ID="PasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="*La contraseña es obligatoria." ToolTip="La contraseña es obligatoria." ValidationGroup="lgnUser"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                        </div>
                        <div class="form-group">
                            <asp:Button CssClass="btn btn-primary" ID="LoginButton" runat="server" CommandName="Login" Font-Names="Verdana" Text="Inicio de sesión" ValidationGroup="lgnUser" />
                        </div>
                        <div class="form-group">
                            <a href="SignUp.aspx"><asp:Label CssClass="badge badge-info" ID="Label1" runat="server" Text="¿No tienes cuenta? ¡Haz clic sobre mí!"></asp:Label></a>
                        </div>
                        <div class="form-group">
                            <asp:Label CssClass="msg-lbl" ID="lblMessage" runat="server" Text="" ForeColor="Blue"></asp:Label>
                        </div>
                    </LayoutTemplate>
                </asp:Login>
            </div>
        </div>
    </form>
</body>
</html>
