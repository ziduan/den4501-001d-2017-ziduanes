﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Reservation.Business;

namespace Reservation.Presentation
{
    public partial class Maintainer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadSports();
            }
        }

        private void LoadSports()
        {
            CollectionManager col = new CollectionManager();
            foreach (var s in col.GetSports())
            {
                ddlSport.Items.Add(new ListItem()
                {
                    Text = s.Description,
                    Value = s.ID.ToString()
                });
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                Business.Reservation r = new Business.Reservation()
                {
                    Rut = (string)Session["RUT"],
                    Date = DateTime.Parse(txtDate.Text),
                    SportID = int.Parse(ddlSport.SelectedValue)
                };
                if (r.Create())
                {
                    lblMessage.Text = "Reservado";
                }
                else
                {
                    lblMessage.Text = "No se pudo reservar";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
    }
}