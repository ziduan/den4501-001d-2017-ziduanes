﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Reservation.Presentation.SignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous" />
    <link href="css/SignUpStyle.css" rel="stylesheet" />
    <link href="css/GlobalStyle.css" rel="stylesheet" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Creación de cuenta</title>
</head>
<body>
    <div class="form-box">
        <form class="signup-form" id="form1" runat="server">
            <div class="container-fluid">
                <div class="form-group">
                    <asp:TextBox placeholder="Rut (ej. 12345678-9)" CssClass="form-control" ID="txtRut" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox placeholder="Primer nombre" CssClass="form-control" ID="txtName" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox placeholder="Apellido paterno" CssClass="form-control" ID="txtLastName" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox placeholder="Nombre de usuario" CssClass="form-control" ID="txtUserName" runat="server"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:TextBox placeholder="Contraseña" CssClass="form-control" ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Button CssClass="btn btn-primary" ID="btnRegister" runat="server" Text="Registrar" OnClick="btnRegister_Click" />
                </div>
                <div class="form-group">
                    <a href="LoginPage.aspx"><asp:Label CssClass="badge badge-info" ID="Label6" runat="server" Text="Volver"></asp:Label></a>
                </div>
            </div>
        </form>
    </div>
</body>
</html>
