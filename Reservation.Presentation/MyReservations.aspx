﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="MyReservations.aspx.cs" Inherits="Reservation.Presentation.MyReservations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"/>
    <link href="css/GlobalStyle.css" rel="stylesheet" />
    <link href="css/MyReservationsStyle.css" rel="stylesheet" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="form-group">
            <asp:Label CssClass="form-lbl" ID="Label3" runat="server" Text="Seleccione una de sus reservas:"></asp:Label>
            <asp:DropDownList CssClass="form-control" ID="ddlMyReservations" runat="server"></asp:DropDownList>
        </div>
        <br />
        <div class="form-group">
            <asp:Label CssClass="form-lbl" ID="Label5" runat="server" Text="Clic en el botón para eliminar la reserva seleccionada"></asp:Label>
            <asp:Button CssClass="btn btn-primary" ID="btnDelete" runat="server" Text="Borrar" OnClick="btnDelete_Click" />
        </div>
        <br />
        <br />
        <div class="form-group">
            <asp:Label CssClass="form-lbl" ID="Label4" runat="server" Text="Modificar reserva seleccionada:"></asp:Label>
            <br />
            <br />
            <asp:Label CssClass="form-lbl" ID="Label1" runat="server" Text="Indique la nueva fecha"></asp:Label>
            <asp:TextBox placeholder="Fecha (ej. dd/MM/yyyy H:mm:ss)" CssClass="form-control" ID="txtDate" runat="server" TextMode="DateTime"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label CssClass="form-lbl" ID="Label2" runat="server" Text="Seleccione un nuevo deporte"></asp:Label>
            <asp:DropDownList CssClass="form-control" ID="ddlSport" runat="server"></asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            <asp:Button CssClass="btn btn-primary" ID="btnUpdate" runat="server" Text="Actualizar" OnClick="btnUpdate_Click" />
        </div>
    </div>










</asp:Content>
