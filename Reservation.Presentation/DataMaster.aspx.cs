﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Reservation.Business;

namespace Reservation.Presentation
{
    public partial class DataMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadReservations();
            }
        }

        private void LoadReservations()
        {
            CollectionManager col = new CollectionManager();
            gdvReservations.DataSource = (
                from r in col.GetReservations()
                join s in col.GetSports() on r.SportID equals s.ID
                select new
                {
                    Fecha = r.Date.ToString(),
                    Deporte = s.Description,
                    Rut = r.Rut
                }).ToList();
            gdvReservations.DataBind();
        }

        protected void gdvReservations_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gdvReservations.PageIndex = e.NewPageIndex;
            LoadReservations();
        }
    }
}