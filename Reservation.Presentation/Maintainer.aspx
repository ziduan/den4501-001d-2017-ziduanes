﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Maintainer.aspx.cs" Inherits="Reservation.Presentation.Maintainer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"/>
    <link href="css/GlobalStyle.css" rel="stylesheet" />
    <link href="css/MaintainerStyle.css" rel="stylesheet" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid">
        <div class="form-group">
            <asp:TextBox CssClass="form-control" ID="txtDate" runat="server" TextMode="DateTime" placeholder="Fecha (ej. dd/MM/yyyy H:mm:ss)"></asp:TextBox>
            <asp:RequiredFieldValidator CssClass="validator-text" ID="rfvDate" runat="server" ErrorMessage="*La fecha es obligatoria" ControlToValidate="txtDate"></asp:RequiredFieldValidator>
        </div>
        <div class="form-group">
            <asp:Label CssClass="form-lbl" ID="Label2" runat="server" Text="Escoja un deporte:"></asp:Label>
            <asp:DropDownList CssClass="form-control" ID="ddlSport" runat="server"></asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            <asp:Button CssClass="btn btn-primary" ID="btnCreate" runat="server" Text="Agregar" OnClick="btnCreate_Click" />
        </div>
    </div>
</asp:Content>
