﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reservation.Business
{
    public class Reservation
    {
        public DateTime Date { get; set; }
        public int SportID { get; set; }
        public string Rut { get; set; }
        public int ID { get; set; }

        public Reservation()
        {

        }

        public Reservation(DateTime date, int sportID, string rut, int iD)
        {
            Date = date;
            SportID = sportID;
            Rut = rut;
            ID = iD;
        }

        public bool Create()
        {
            try
            {
                Data.Reservation r = new Data.Reservation()
                {
                    Rut = Rut,
                    Date = Date,
                    SportID = SportID
                };
                if (CanInsert())
                {
                    Connection.DataBase.Reservation.Add(r);
                    Connection.DataBase.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Read()
        {
            try
            {
                Data.Reservation r = Connection.DataBase.Reservation.First(s => s.Date == Date && s.SportID == SportID);
                Date = r.Date;
                SportID = r.SportID;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(DateTime newDate, int newSportId)
        {
            try
            {
                Data.Reservation r = Connection.DataBase.Reservation.First(s => s.Date == Date && s.SportID == SportID);
                r.Date = newDate;
                r.SportID = newSportId;
                if (CanInsert())
                {
                    Connection.DataBase.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete()
        {
            try
            {
                Data.Reservation r = Connection.DataBase.Reservation.First(s => s.Date == Date && s.SportID == SportID);
                Connection.DataBase.Reservation.Remove(r);
                Connection.DataBase.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string Description()
        {
            Sport s = new Sport()
            {
                ID = SportID
            };
            s.ReadThroughId();
            return string.Format("{0}_{1}", Date, s.Description);
        }

        public bool CanInsert()
        {
            CollectionManager col = new CollectionManager();
            foreach (var d in col.GetReservations())
            {
                if (d.Date == Date)
                {
                    if (d.SportID == SportID)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
