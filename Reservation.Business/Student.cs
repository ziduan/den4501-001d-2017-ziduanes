﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reservation.Data;

namespace Reservation.Business
{
    public class Student
    {
        public string Rut { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }

        public Student()
        {

        }

        public Student(string rut, string fName, string lName)
        {
            Rut = rut;
            FName = fName;
            LName = lName;
        }

        public bool Create()
        {
            try
            {
                Data.Student stu = new Data.Student()
                {
                    Rut = Rut,
                    FName = FName,
                    LName = LName
                };
                Connection.DataBase.Student.Add(stu);
                Connection.DataBase.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Read()
        {
            try
            {
                Data.Student stu = Connection.DataBase.Student.First(s => s.Rut == Rut);
                FName = stu.FName;
                LName = stu.LName;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
