﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reservation.Business
{
    public class CollectionManager
    {
        public List<Reservation> GetReservations()
        {
            List<Reservation> output = new List<Reservation>();
            foreach (Data.Reservation r in Connection.DataBase.Reservation)
            {
                output.Add(new Reservation()
                {
                    Rut = r.Rut,
                    ID = r.ID,
                    Date = r.Date,
                    SportID = r.SportID
                });
            }
            return output;
        }

        public List<Sport> GetSports()
        {
            List<Sport> output = new List<Sport>();
            foreach (Data.Sport s in Connection.DataBase.Sport)
            {
                output.Add(new Sport()
                {
                    ID = s.ID,
                    Description = s.Description
                });
            }
            return output;
        }

        public List<Reservation> GetMyReservations(string rut)
        {
            List<Reservation> output = new List<Reservation>();
            foreach (Data.Reservation r in Connection.DataBase.Reservation)
            {
                if (rut.CompareTo(r.Rut) == 0)
                {
                    output.Add(new Reservation()
                    {
                        ID = r.ID,
                        Rut = r.Rut,
                        Date = r.Date,
                        SportID = r.SportID
                    });
                }

            }
            return output;
        }

        /*public List<var> GetReservationGdv() 
        {
            return (
                from r in Connection.DataBase.Reservation
                join s in Connection.DataBase.Sport on r.SportID equals s.ID
                select new
                {
                    Fecha = r.Date.ToString(),
                    Deporte = s.Description,
                    Rut = r.Rut
                }).ToList();
        }*/
    }
}
