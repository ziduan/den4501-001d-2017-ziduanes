﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reservation.Data;

namespace Reservation.Business
{
    internal class Connection
    {
        private static DB_ZIDUANES_RESERVATIONSEntities1 _dataBase;

        public static DB_ZIDUANES_RESERVATIONSEntities1 DataBase
        {
            get
            {
                if (_dataBase == null)
                {
                    _dataBase = new DB_ZIDUANES_RESERVATIONSEntities1();
                }
                return _dataBase;
            }
            set
            {
                _dataBase = value;
            }
        }

    }
}
