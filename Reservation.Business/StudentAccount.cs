﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reservation.Business
{
    public class StudentAccount
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string RUT { get; set; }

        public StudentAccount()
        {

        }

        public StudentAccount(string username, string password, string rut)
        {
            Username = username;
            Password = password;
            RUT = rut;
        }

        public bool Create()
        {
            try
            {
                Data.StudentAccount stu = new Data.StudentAccount()
                {
                    RUT = RUT,
                    Username = Username,
                    Password = Password
                };

                Connection.DataBase.StudentAccount.Add(stu);
                Connection.DataBase.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Authenticate()
        {
            try
            {
                Data.StudentAccount user = Connection.DataBase.StudentAccount.First(u => u.Username == Username && u.Password == Password);
                RUT = user.RUT;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
