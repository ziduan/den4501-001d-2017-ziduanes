﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reservation.Business
{
    public class Sport
    {
        public int ID { get; set; }
        public string Description { get; set; }

        public Sport()
        {

        }

        public Sport(int iD, string description)
        {
            ID = iD;
            Description = description;
        }

        public bool ReadThroughId()
        {
            try
            {
                Data.Sport s = Connection.DataBase.Sport.First(p => p.ID == ID);
                Description = s.Description;
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        public bool ReadThroughDescription()
        {
            try
            {
                Data.Sport s = Connection.DataBase.Sport.First(p => p.Description == Description);
                ID = s.ID;
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
